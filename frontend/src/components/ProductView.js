import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col, } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import EditModal from "./EditModal";

export default function ProductView() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const { productId } = useParams();
  console.log(productId)

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  const buy = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name,
        description,
        price,
        // productId: productId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Successfully bought",
            icon: "success",
            text: "You have successfully bought for this product.",
          });

          navigate("/products");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  function fetchProduct(){
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }

  useEffect(() => {
    console.log(productId);

    fetchProduct()

    
  }, [productId]);

  
  return (
    
    <Container>
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PHP {price}</Card.Text>


              {user.isAdmin ? (
                <EditModal name={name} description={description} price={price} productId={productId} fetchProduct={fetchProduct} />
              ) : user.id !== null ? (
                <Button
                  variant="primary"
                  className="m-4"
                  onClick={() => buy(productId)}
                >
                  Buy Now
                </Button>
              ) : (
                <Button className="btn btn-danger m-4" as={Link} to="/login">
                  Log in to Buy
                </Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
