import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Swal from "sweetalert2";

export default function EditModal({name, description, price, productId, fetchProduct}) {
  console.log(productId)
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [newName, setNewName] = useState(name); 
  const [newDescription, setNewDescription] = useState(description);
  const [newPrice, setNewPrice] = useState(price);

  function updateProduct(productId) {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: newName,
        description: newDescription,
        price: newPrice
      })
    })
      .then((res) => res.json())
      .then((data) => {
        Swal.fire({
          title: "Successfully updated",
          icon: "success",
          text: "You have successfully updated this product.",
        });
        setNewName("")
        setNewDescription("")
        setNewPrice("")
        handleClose()
        fetchProduct()
      });
  }

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Edit
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Product?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicName">
              <Form.Label>Name:</Form.Label>
              <Form.Control type="string" value={newName} onChange={e => {
                setNewName(e.target.value)
              }} />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicDescription">
              <Form.Label>Description:</Form.Label>
              <Form.Control type="string" value={newDescription} onChange={e => {
                setNewDescription(e.target.value)
              }} />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPrice">
              <Form.Label>Price:</Form.Label>
              <Form.Control type="number" value={newPrice} onChange={e => {
                if(e.target.value <= 0){
                    return
                }
                setNewPrice(e.target.value)
              }} />
            </Form.Group>

            {/* <Button variant="primary" type="submit">
              Submit
            </Button> */}
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={e => updateProduct(productId)}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
