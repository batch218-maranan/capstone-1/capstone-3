import { useState, useEffect } from "react";
import { Card, Button } from "react-bootstrap";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default function ProductCard({ product }) {
  const { name, description, price, _id } = product;


  return (
    <div className="d-inline-flex m-2 justify-content-around text-center">

    <Card style={{ width: '18rem', background: '#d4fade' }}>
      <Card.Body className="p-2">
        <Card.Title className="pb-4">{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>₱ {price}</Card.Text>
        <Button className="bg-primary w-100" as={Link} to={`/products/${_id}`}>
          Details
        </Button>
      </Card.Body>
    </Card>
    </div>

  );
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
