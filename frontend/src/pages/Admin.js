import { useState, useEffect } from "react";
import { Row, Col, Button } from "react-bootstrap";

import { useNavigate, Link } from "react-router-dom";

import Table from "react-bootstrap/Table";

import AddProduct from "../components/AddProduct";

export default function Admin() {


  const navigate = useNavigate();

  const [products, setProducts] = useState([]);

  const headers = [{name: 'Name'}, {description: 'Description'}, {price: 'Price'}, {isActive:'Availability'}, {actions: 'Action'}]


  function getProducts() {
      fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
          method: "GET",
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
      })
      .then(res => res.json())
      .then(data => {
          console.log(data);

          setProducts(data);

      })
  }

  useEffect(()=> {
      getProducts();

  },[])

  function archiveProduct(productId, option) {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
    })
    .then(res => res.json())
    .then(data => {

        getProducts();
    })

}



  return (
    
    <>
      <>
        <Row className="m-4 justify-content-center">
          <h1 className="text-center">Admin Dashboard</h1>
          <Col className="text-center col-4 col-md-2">
            <AddProduct />
          </Col>

          <Col className="text-center col-4 col-md-2">
            <Button as={Link} to={"/admin"} variant="success">
              Show All Products
            </Button>
          </Col>
        </Row>

        <Row>
          <Table striped bordered hover responsive>
            <thead>
              <tr className="text-center">
                {headers.map((header) => {
                  return <th>{Object.values(header)[0]}</th>;
                })}
              </tr>
            </thead>
            <tbody>
              {products.map((product) => {
                return (
                  <tr
                    className={`${
                      product.isActive ? "" : ""
                    } align-middle`}
                  >
                    <th>{product.name}</th>
                    <td>{product.description}</td>
                    <td className="text-end">
                      &#8369;{" "}
                      {Intl.NumberFormat("en-US", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      }).format(product.price)}
                    </td>
                    <td className={`text-center`}>
                      <strong>
                        {product.isActive ? "Active" : "Inactive"}
                      </strong>
                    </td>
                    <td className="text-center">
                      <Button
                        className="my-1 py-0 px-2 d-block mx-auto"
                        onClick={(e) => {
                          navigate(`/products/${product._id}`);
                        }}
                      >
                        Update
                      </Button>

                      

                      <Button className="my-1 py-0 px-2 d-block mx-auto bg-danger"
                                onClick={e => {
                                    const option = product.isActive ? 'deactivate' : 'activate';
                                    archiveProduct(product._id, option)
                                }}
                                >     
                                    {product.isActive ? 'Deactivate' : 'Activate'}
                                </Button>


                      
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Row>
      </>
    </>
  );
}
