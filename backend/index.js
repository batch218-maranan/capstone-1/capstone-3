/*
Git commands:
	npm init -y
	npm install express mongoose
	touch .gitignore >> content node_modules
*/

// dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require ("cors");

// ROUTERS
const userRoutes = require ("./routes/userRoutes.js");
const productRoutes = require ("./routes/productRoutes.js");


// Server setup
const app = express();


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);


// DB connection
mongoose.connect("mongodb+srv://user:user@capstone-2.bu8e0iz.mongodb.net/capstone-2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

mongoose.connection.once('open', () => console.log('Now connected to Maranan-Mongo DB Atlas'));

app.listen(process.env.PORT || 5000, () => 
{console.log(`API is now online on port ${process.env.PORT || 5000}`)
});