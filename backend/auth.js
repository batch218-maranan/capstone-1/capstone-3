const jwt = require("jsonwebtoken"); // jsonwebtoken - package for web token access
const secret = "Capstone-2";

module.exports.createAccessToken = (user) => {
	// payload - included data in our document
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

								  //callback function		
	return jwt.sign(data, secret, {/*expiresIn: "60s"*/});
}

// To verify a token from the request (from postman)
module.exports.verify=(request, response, next) => {

	// GET JWT (JSON web token) from postman
	let token = request.headers.authorization

	if (typeof token!== "undefined") {
		 console.log(token);
		 // remove unecessary characters ("Bearer "") from the token
		 token = token.slice(7, token.length);

		 return jwt.verify(token, secret, (error, data) => {
		 	if (error) {
		 		return response.send ({
		 			auth: "Failed."
		 		})
		 	} else {
		 		next()
		 	}
		 })
	}
	else {
		return null
	}
}

// to decide the user details from the token
module.exports.decode = (token) => {

	if (typeof token !== "undefined") {

		// remove first 7 characters ("Bearer.").from.tje.token
		token = token.slice(7, token.length);

	}
	return jwt.verify(token, secret, (error, data) => {
		if (error) {
			return null
		} else {
			return jwt.decode(token, {complete:true}).payload
		}
	})

}
